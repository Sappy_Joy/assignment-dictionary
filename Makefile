NAME=dict

SOURCES=$(wildcard src/*.asm)
OBJECTS=$(SOURCES:src/%.asm=build/%.o)

ASM=nasm
LD=ld
RM=rm -rf
ASMFLAGS := -g -felf64 -Iinclude/

all: $(NAME)

$(NAME): $(OBJECTS)
	$(LD) $^ -o $@

build/%.o: src/%.asm Makefile | build
	$(ASM) $(ASMFLAGS) $< -o $@

build:
	mkdir -p $@

clean:
	$(RM) $(NAME) build

gdb: $(NAME)
	gdb $(NAME) -ex "layout asm" -ex "layout regs"

.PHONY: all clean gdb