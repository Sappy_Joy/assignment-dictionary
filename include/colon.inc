%define head 0      ; list start address

%macro colon 2
%ifid %2
    %2: dq head
    %define head %2
%else
    %fatal "The second argument must be id"
%endif
%ifstr %1
    db %1, 0
%else
    %fatal "First argument must be string"
%endif
%endmacro