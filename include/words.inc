%include "colon.inc"

; Movies I like by year

section .data

colon "1995", seven
db "Se7en", 0

colon "1996", fargo
db "Fargo", 0

colon "1997", lost_highway
db "Lost Highway", 0

colon "1998", you_ve_got_mail
db "You've Got Mail", 0

colon "1999", american_beauty
db "American Beauty", 0

colon "2000", cast_away
db "Cast Away", 0

colon "2001", darko
db "Donnie Darko", 0

colon "2002", later
db "28 Days Later...", 0

colon "2003", big_fish
db "Big Fish", 0

colon "2004", terminal
db "The Terminal", 0

colon "2005", smith
db "Mr. & Mrs. Smith", 0

colon "2006", happyness
db "The Pursuit of Happyness", 0

colon "2007", wild
db "Into The Wild", 0

colon "2008", seven_pounds
db "Seven Pounds", 0

colon "2009", shutter_island
db "Shutter Island", 0

colon "2010", social_network
db "The Social Network", 0

colon "2011", time
db "Time", 0

colon "2012", hunt
db "The Hunt", 0

colon "2013", whiplash
db "Whiplash", 0

colon "2014", fool
db "Дурак", 0

colon "2015", big_shot
db "The Big Shot", 0

colon "2016", la_la_land
db "La La Land", 0

colon "2017", three_billboards
db "Three Billboards Outside Ebbing, Missouri", 0

colon "2018", super_squirrels
db "Super Squirrels", 0

colon "2019", give_me_wings
db "Donne-moi des ailes", 0

colon "2020", john_wilson
db "How to with John Wilson", 0

colon "2021", moby_doc
db "Moby Doc", 0

colon "2022", thursday
db "A Thursday", 0