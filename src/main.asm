%include "lib.inc"
%include "colon.inc"
%include "dict.inc"
%include "words.inc"


%define RC_KEY_NOT_FOUND 1
%define RC_TOO_LONG_KEY 2

%define BUFFER_SIZE 256

global _start

section .data
    prompt:         db "Year: ", 0
    too_long_key:   db "ERROR: Key is too long", EOL, 0
    not_found_err:  db "ERROR: Key is not found", EOL, 0

section .text
_start:
    mov     rdi, prompt
    call    print_string
    mov     rsi, BUFFER_SIZE
    sub     rsp, BUFFER_SIZE
    mov     rdi, rsp
    call    read_word
    test    rax, rax
    jz      .too_long

    mov     rsi, head
    mov     rdi, rax
    call    find_word

    test    rax, rax
    jz      .no_key

    add     rax, 8
    mov     rdi, rax
    push    rdi
    call    string_length
    pop     rdi

    inc     rax
    add     rdi, rax

    call    print_string
    call    print_newline
    
    xor     rdi, rdi
    call    exit

.too_long:
    mov     rdi, too_long_key
    call    print_string
    call    print_newline
    mov     rdi, RC_TOO_LONG_KEY
    call    exit
        
.no_key:
    mov     rdi, not_found_err
    call    print_string
    call    print_newline
    mov     rdi, RC_KEY_NOT_FOUND
    call    exit